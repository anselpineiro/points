var mongoose = require('mongoose');

var TransactionSchema = mongoose.Schema({
    payer: String,
    points: Number,
    timestamp: Date,
    availablePoints: Number,
    specificPayer: Boolean
}, { _id: false });

var Transaction = mongoose.model('Transaction', TransactionSchema);

module.exports = Transaction;