'use strict';
var http = require('http');
var port = process.env.PORT || 1337;



var express = require('express');
var app = express();

const index = require('./routes/index');
const balance = require('./routes/balance');
const add = require('./routes/add');
const spend = require('./routes/spend');

const bodyParser = require('body-parser')
app.use(express.json());
app.use(bodyParser.json());



app.use('/', index);
app.use('/balance', balance);
app.use('/add', add);
app.use('/spend', spend);


app.listen(port, function () {
    console.log('Listening to Port ' + port);
});
