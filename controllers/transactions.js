var transactions = [];
module.exports.transactions = transactions;

//Stores spending from non specified payers
var nonSpecificSpendLog = [];

var Transaction = require('../models/transaction');
var Validator = require('./validator');


exports.add = function (req, res) {

    if (!Validator.validateAdd(req.body)) {
        res.status(400).send("Invalid input");        
        return;
    }

    transaction = new Transaction(req.body);
    transaction.availablePoints = transaction.points;


    if (transaction.points < 0) {
        spendPointsReq(req, res);
        return;
    }

    transactions.push(transaction);

    if (hasFollowingNegatives(transaction)) 
        reprocessTransactions(transaction);
    
    res.send(transaction);

}

exports.balance = function (req, res) {
    
    if (transactions.length == 0) {
        res.send({});
        return;
    }
    var balance = getBalance();

    res.send(balance);
}

exports.spend = function (req, res) {

    if (!Validator.validateSpend(req.body)) {
        res.status(400).send("Invalid Input");
        return;
    }

    spendPointsReq(req, res);

}

exports.total = function (req, res) {
    res.send(JSON.stringify(transactions));
}

function getBalance() {

    var balance = {};
    transactions.forEach(function(element) {
        if (balance[element.payer] == undefined)
            balance[element.payer] = 0;

        balance[element.payer] += element.points;
    });
    return balance;
}


function getTotalBalance() {

    if (transactions.length == 0)
        return 0;

    return transactions.reduce(function (a, b) { return { points: a.points + b.points }; }).points;
}

function hasFollowingNegatives (transaction) {

    var precedesNegatives = false;    

    transactions.some(function (element) {

        if (element.payer != transaction.payer || element.points <= 0)
            return false;

        if (element.timestamp.getTime() > transaction.timestamp.getTime())
            precedesNegatives = true;

        return precedesNegatives;
    });

    return precedesNegatives;
}

//Sort transactions by date.  Oldest first
function sortTransactions() {
    transactions.sort(function (a, b) { return a.timestamp.getTime() - b.timestamp.getTime() })
}


function reprocessTransaction(element, startDate) {
    return element.timestamp.getTime() >= startDate.getTime();
}

//Reprocess transactions for a given payer starting from a specific date
function reprocessTransactions(initialTransaction) {

    
    var startDate = initialTransaction == undefined ? undefined : initialTransaction.timestamp;

    sortTransactions();

    //Transaction with negative value and no specified payer.
    //These get removed and replaced.
    var removedTransactions = [];


    if (initialTransaction == undefined) {
        //Reset everything if negative
        transactions.forEach(function (element) {
            if (element.points < 0 && element.specificPayer == false)
                removedTransactions.push(element);

            element.availablePoints = element.points;
        })
    }
    else if (initialTransaction.points > 0) {
        //Reset transactions available points that are dated after this transaction if the points are negative and not specific to a payer
        transactions.forEach(function (element) {
            if (reprocessTransaction(element, startDate)) {
                if (element.points < 0 && element.specificPayer == false)
                    removedTransactions.push(element);

                element.availablePoints = element.points;

            }
        })
    }

    //Remove the specified negative transactions from the collection
    if (removedTransactions.length > 0) {
        var rebuiltTransactions = [];
        transactions.forEach(function (element) {
            if (!removedTransactions.includes(element))
                rebuiltTransactions.push(element);
        })

        transactions = rebuiltTransactions;
    }


    //Reprocess transactions that happened after the start date, excluding non specific payer negative transactions.
    for (i = 0; i < transactions.length; i++) {
        transaction = transactions[i];

        if (initialTransaction != undefined && initialTransaction.points > 0 && !reprocessTransaction(transaction, startDate))
            continue;

        //No need to reprocess this transaction if there are no points or available points
        if (transaction.points <= 0 || transaction.availablePoints <= 0)
            continue;


        //Find transactions with the same payer with negative points
        for (j = i; j < transactions.length; j++) {
            transactionJ = transactions[j];
            if (transaction.payer != transactionJ.payer || transactionJ.points >= 0)
                continue;

            if (transactionJ.availablePoints != undefined && transactionJ.availablePoints >= 0)
                continue;

            if (transactionJ.availablePoints == undefined)
                transactionJ.availablePoints = transactionJ.points;


            if (transaction.availablePoints >= Math.abs(transactionJ.availablePoints)) {
                transaction.availablePoints += transactionJ.availablePoints;
                transactionJ.availablePoints = 0;
            }
            else {
                transactionJ.availablePoints += transaction.availablePoints;
                transaction.availablePoints = 0;
                break;
            }
        }
    }

    var log = nonSpecificSpendLog;
    nonSpecificSpendLog = [];
    //Replace removed transactions with new transactions
    log.some(function (element) {
        if (startDate == undefined || element.timestamp.getTime() >= startDate.getTime())
            spendPoints(element.points);
    });

}



function spendPoints(points, payer, date) {


    if (date == undefined)
        date = new Date();

    var specificPayer = payer != undefined;
    if (!specificPayer) 
        nonSpecificSpendLog.push({ "points": points, "timestamp": date });

    var debit = [];
    var balance = getBalance();




    transactions.some(function (element) {

        if (payer != undefined && payer != element.payer)
            return false;

        //If transaction is less than 0 or balance from the payer is 0, don't subtract anything more        
        if (element.availablePoints <= 0 || balance[element.payer] <= 0)
            return false;


        debitItem = undefined;

        debit.some(function (debitElement) {

            if (debitElement.payer != element.payer)
                return false;

            debitItem = debitElement;
            return true;
        });


        if (debitItem == undefined) {
            debitItem = { "payer": element.payer, "points": 0 }
            debit.push(debitItem);
        }


        var deduction = (function () {
            if (element.availablePoints >= points)
                deduct = points
            else
                deduct = element.availablePoints;
            return deduct;
        })();


        element.availablePoints -= deduction;
        debitItem.points -= deduction;
        balance[element.payer] -= deduction;
        points -= deduction;


        if (points <= 0)
            return true;
    });

    debit.forEach(function (element) {
        transaction = new Transaction({
            "payer": element.payer,
            "points": element.points,
            "timestamp": date,
            "availablePoints": 0,
            "specificPayer": specificPayer
        });
        transactions.push(transaction);
    })

    if (specificPayer)
        reprocessTransactions();

    return debit;
}

function spendPointsReq(req, res) {

    var points = Math.abs(req.body.points);



    //Prevent overall point balance from being negative
    if (points > getTotalBalance()) {
        res.send("Insufficient Points");
        return;
    }

    //If a payer is specified, points only get deducted from transactions that payer's balance
    var payer = req.body.payer;
    var date = req.body.timestamp == undefined ? new Date() : new Date(req.body.timestamp);


    //Sort transactions by date
    sortTransactions();
    
    var debit = spendPoints(points, payer, date);

    res.send(debit);
}