

exports.validateAdd = function (body) {
    return body != undefined && isString(body.payer) && isNumber(body.points) && isDate(body.timestamp);
};

exports.validateSpend = function (body) {
    return body != undefined && isNumber(body.points);
};


function isNumber(key) {
    return isType(key, "number");
}

function isDate(key) {
    if (key == undefined)
        return false;

    var date = new Date(key);

    return !isNaN(date.getTime());
}

function isString(key) {
    return isType(key, "string");
}

function isType(key, type) {
    return key != undefined && typeof key == type;
}