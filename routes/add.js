const express = require('express')
let app = express.Router()

var transactions = require('../controllers/transactions');

app.post('/', transactions.add);

module.exports = app
