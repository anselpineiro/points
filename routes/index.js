const express = require('express')
let app = express.Router()

var transactions = require('../controllers/transactions');

app.get('/', transactions.total);

module.exports = app