# Points
## Requirements
NodeJS [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
## Run Site:
In the repo directory, run the following commands:
```
npm install
node .\server
```
Site will run on localhost:1337

## Change port
Edit line 3 on server.js or create .env file in repo directory with the following key.
```
port=PORTNUMBER
```